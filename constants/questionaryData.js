import shuffle from 'lodash.shuffle';

const questions = [
  {
    question: 'What is the best animal in the world?',
    answers: [
      'fox',
      'cow',
      'giraffe',
      'lion'
    ],
    correctAnswer: 0,
  },
  {
    question: 'What is the worst animal in the world?',
    answers: [
      'all',
      'human',
      'tick',
      'mosquito'
    ],
    correctAnswer: 3,
  },
  {
    question: 'red or blue',
    answers: [
      'red',
      'blue',
    ],
    correctAnswer: 0,
  }
];

export const generateQuestionnaire = () => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(shuffle(questions).slice(0, 2));
    }, 3000);
  });
}
