import * as React from 'react';
import { Button, Text, StyleSheet, View } from 'react-native';
import { routes } from '../../routes';

const HomeScreen = ({ navigation }) => {
  const startQuiz = () => {
    console.log('lesson started!')
    routes.goToQuiz(navigation);
  };

  return (
    <View style={styles.container}>
      <Text>Welcome to the ultimate quiz!</Text>
      <View style={styles.buttonsContainer}>
        <Button
          title="Click to start the quiz"
          onPress={startQuiz}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    justifyContent: 'space-between',
    flex: 1,
    backgroundColor: '#FFF',
  },
  buttonsContainer: {
    minHeight: 48,
    marginBottom: 48,
  },
});

export default HomeScreen;
