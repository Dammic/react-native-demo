import * as React from 'react';
import { Button, Text, StyleSheet, View } from 'react-native';
import { routes } from '../../../routes';
import { Answers } from '../components/Answers';

const Summary = ({ navigation, answers, questionnaire }) => {
  return (
    <View style={styles.container}>
      <Text>Here is a summary of your quiz:</Text>

      <View style={styles.answers}>
        {answers.map((answer, index) => (
          <Answers
            key={index}
            question={questionnaire[index]}
            questionNr={index}
            actualAnswerNr={answer}
            correctAnswerNr={questionnaire[index].correctAnswer}
          />
        ))}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flex: 1,
    backgroundColor: '#FFF',
  },
  answers: {
    marginTop: 24,
  },
});

export default Summary;
