import React from 'react';
import { Button, Text, StyleSheet, View, TouchableOpacity } from 'react-native';
import { Answer } from './components/Answer';

const Answers = ({ question, questionNr, onSelect, actualAnswerNr, correctAnswerNr }) => {
  return (
    <View style={styles.answersContainer}>
      <Text style={styles.questionText}>{`[${questionNr + 1}]: ${question.question}`}</Text>
      {question.answers.map((answerText, index) => (
        <Answer
          key={index}
          index={index}
          onSelect={onSelect}
          correctAnswerNr={correctAnswerNr}
          actualAnswerNr={actualAnswerNr}
          answerText={answerText}
        />
      ))}
    </View>
  );
};

const styles = StyleSheet.create({
  answersContainer: {
    display: 'flex',
  },
});

export default Answers;
