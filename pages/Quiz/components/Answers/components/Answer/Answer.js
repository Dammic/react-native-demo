import React from 'react';
import { Text, StyleSheet, View, TouchableOpacity } from 'react-native';

export { default as Answer } from './Answer';

const Answer = ({ onSelect, index, correctAnswerNr, actualAnswerNr, answerText }) => {
  const textStyle = [
    index === actualAnswerNr && actualAnswerNr !== correctAnswerNr ? styles.badAnswer : '',
    index === correctAnswerNr ? styles.correctAnswer : '',
  ];

  if (!onSelect) {
    return (
      <Text key={answerText} style={textStyle}>
        {answerText}
      </Text>
    )
  }

  return (
    <TouchableOpacity
      key={answerText}
      style={styles.answer}
      onPress={() => onSelect(index)}>
      <Text>{answerText}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  answer: {
    width: '50%',
  },
  questionText: {
    marginBottom: 30,
  },
  correctAnswer: {
    backgroundColor: '#008000',
  },
  badAnswer: {
    backgroundColor: 'red',
  },
});

export default Answer;
