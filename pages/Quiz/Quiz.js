import * as React from 'react';
import { ActivityIndicator, Button, Text, StyleSheet, View, TouchableOpacity } from 'react-native';
import { Answers } from './components/Answers';
import { routes } from '../../routes';
import { generateQuestionnaire } from '../../constants/questionaryData';
import { Summary } from './Summary';

const Quiz = ({ navigation }) => {
  const [questionnaire, setQuestionnaire] = React.useState({});
  const [isQuestionnaireLoading, setQuestionnaireLoading] = React.useState(true);
  const [currentQuestionNr, setCurrentQuestionNr] = React.useState(0);
  const [answers, setAnswer] = React.useState([]);

  React.useEffect(() => {
    async function fetchQuestionnaire() {
      const questionnaire = await generateQuestionnaire();
      console.log(questionnaire);
      setQuestionnaire(questionnaire);
      setQuestionnaireLoading(false);
    };

    fetchQuestionnaire();
  }, []);

  const goToNextQuestion = (answer) => {
    const newAnswers = [...answers, answer];
    setAnswer(newAnswers);

    setCurrentQuestionNr(currentQuestionNr + 1);
  };

  if (isQuestionnaireLoading) {
    return (
      <View style={[styles.container, styles.loader]}>
        <ActivityIndicator size="large" color="#0000ff" />
      </View>
    );
  }

  if (!questionnaire[currentQuestionNr]) {
    return (
      <Summary
        navigation={navigation}
        answers={answers}
        questionnaire={questionnaire}
      />
    );
  }

  return (
    <View style={styles.container}>
      <Answers
        question={questionnaire[currentQuestionNr]}
        questionNr={currentQuestionNr}
        onSelect={goToNextQuestion}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    display: 'flex',
  },
  loader: {
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default Quiz;
